/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.file_input_output_new;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author f
 */
public class Computer implements Serializable {

    private int hand; //hand: 0=Rock, 1=Scissors, 2=Paper
    private int PlayerHand;//PlayerHand: 0=Rock, 1=Scissors, 2=Paper
    private int win, lose, draw, status;

    public Computer() {
    }

    private int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int PaoYingChoob(int PlayerHand) {
        this.PlayerHand = PlayerHand;
        this.hand = choob();
        if (this.PlayerHand == this.hand) {
            draw++;
            status = 0;
            return 0;
        }
        if (this.PlayerHand == 0 && this.hand == 1 || this.PlayerHand == 1 && this.hand == 2
                || this.PlayerHand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return PlayerHand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
}
