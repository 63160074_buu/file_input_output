/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.file_input_output_new;

import java.io.Serializable;

/**
 *
 * @author f
 */
public class Dog implements Serializable {

    private String Name;
    private int Age;

    public Dog(String Name, int Age) {
        this.Name = Name;
        this.Age = Age;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    @Override
    public String toString() {
        return "Dog{" + "Name=" + Name + ", Age=" + Age + '}';
    }

}
