/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.file_input_output_new;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author f
 */
public class Test_File_Input {

    public static void main(String[] args) {
        FileInputStream fis = null;
        Dog dog = null;
        Rectangle rec = null;
        ObjectInputStream ois = null;
        try {
            File file = new File("dog.obj");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);

            dog = (Dog) ois.readObject();
            rec = (Rectangle) ois.readObject();
            System.out.println(dog);
            System.out.println(rec);

            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!!!!!");
        } catch (IOException ex) {
            Logger.getLogger(Test_File_Input.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Test_File_Input.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (NullPointerException ex) {
                System.out.println("Null Pointer Exception!!!!!");
            } catch (IOException ex) {
                Logger.getLogger(Test_File_Input.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
